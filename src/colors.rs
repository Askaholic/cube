pub const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
pub const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];

pub const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
pub const BLUE: [f32; 4] = [0.0, 0.0, 1.0, 1.0];
pub const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];

pub fn scale_color(color: &[f32; 4], brightness: f32) -> [f32; 4] {
    [
        color[0] * 0.5 * (1.0 + brightness),
        color[1] * 0.5 * (1.0 + brightness),
        color[2] * 0.5 * (1.0 + brightness),
        color[3],
    ]
}
