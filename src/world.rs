use std::f64::consts::PI;
use std::ops::Deref;

use crate::math::{Matrix, Point};
use crate::models::{Model, Triangle, Vertex};

const PI2: f64 = PI * 2.0;

pub struct Coordinate(Point);

impl Deref for Coordinate {
    type Target = Point;

    fn deref(&self) -> &Point {
        &self.0
    }
}

pub struct Entity {
    location: Coordinate,
    thetax: f64,
    thetay: f64,
    thetaz: f64,
    model: Model,
}

impl Entity {
    pub fn new(x: f64, y: f64, z: f64, model: Model) -> Entity {
        Entity {
            location: Coordinate(Point::new(x, y, z)),
            thetax: 0.0,
            thetay: 0.0,
            thetaz: 0.0,
            model,
        }
    }

    pub fn update(&mut self, _ticks: usize) {
        self.thetax += 0.001;
        self.thetay += 0.002;
        self.thetaz += 0.003;

        if self.thetax >= PI2 {
            self.thetax -= PI2;
        }
        if self.thetay >= PI2 {
            self.thetay -= PI2;
        }
        if self.thetaz >= PI2 {
            self.thetaz -= PI2;
        }
    }

    pub fn triangles(&self) -> Vec<Triangle> {
        let rot_x = Matrix::new(&[
            &[1.0, 0.0, 0.0],
            &[0.0, self.thetax.cos(), -self.thetax.sin()],
            &[0.0, self.thetax.sin(), self.thetax.cos()],
        ]);
        let rot_y = Matrix::new(&[
            &[self.thetay.cos(), 0.0, self.thetay.sin()],
            &[0.0, 1.0, 0.0],
            &[-self.thetay.sin(), 0.0, self.thetay.cos()],
        ]);
        let rot_z = Matrix::new(&[
            &[self.thetaz.cos(), -self.thetaz.sin(), 0.0],
            &[self.thetaz.sin(), self.thetaz.cos(), 0.0],
            &[0.0, 0.0, 1.0],
        ]);
        let rot = &(&rot_x * &rot_y) * &rot_z;
        let [worldx, worldy, worldz] = self.location.as_slice();

        self.model
            .triangles()
            .iter()
            .map(|t| {
                let mut vec: Vec<Vertex> = t
                    .vertices()
                    .iter()
                    .map(|vertex| {
                        let [vx, vy, vz] = vertex.as_slice();
                        let vector = Matrix::new(&[&[*vx], &[*vy], &[*vz]]);
                        let result = &rot * &vector;
                        Vertex::new(result[0][0], result[1][0], result[2][0])
                    })
                    .collect();
                let (v3, v2, v1) = (vec.pop().unwrap(), vec.pop().unwrap(), vec.pop().unwrap());
                Triangle::new([v1, v2, v3])
            })
            .map(|t| t.translate(*worldx, *worldy, *worldz))
            .collect()
    }
}

pub fn cube(x: f64, y: f64, z: f64, w: f64) -> Entity {
    let l = w * 0.5;
    Entity::new(
        x,
        y,
        z,
        Model::new(
            l,
            l,
            l,
            &[
                // North
                Triangle::new([
                    Vertex::new(w, 0.0, w),
                    Vertex::new(w, w, w),
                    Vertex::new(0.0, w, w),
                ]),
                Triangle::new([
                    Vertex::new(w, 0.0, w),
                    Vertex::new(0.0, w, w),
                    Vertex::new(0.0, 0.0, w),
                ]),
                // East
                Triangle::new([
                    Vertex::new(w, 0.0, 0.0),
                    Vertex::new(w, w, 0.0),
                    Vertex::new(w, w, w),
                ]),
                Triangle::new([
                    Vertex::new(w, 0.0, 0.0),
                    Vertex::new(w, w, w),
                    Vertex::new(w, 0.0, w),
                ]),
                // South
                Triangle::new([
                    Vertex::new(0.0, 0.0, 0.0),
                    Vertex::new(0.0, w, 0.0),
                    Vertex::new(w, w, 0.0),
                ]),
                Triangle::new([
                    Vertex::new(0.0, 0.0, 0.0),
                    Vertex::new(w, w, 0.0),
                    Vertex::new(w, 0.0, 0.0),
                ]),
                // West
                Triangle::new([
                    Vertex::new(0.0, 0.0, w),
                    Vertex::new(0.0, w, w),
                    Vertex::new(0.0, w, 0.0),
                ]),
                Triangle::new([
                    Vertex::new(0.0, 0.0, w),
                    Vertex::new(0.0, w, 0.0),
                    Vertex::new(0.0, 0.0, 0.0),
                ]),
                // Top
                Triangle::new([
                    Vertex::new(0.0, w, 0.0),
                    Vertex::new(0.0, w, w),
                    Vertex::new(w, w, w),
                ]),
                Triangle::new([
                    Vertex::new(0.0, w, 0.0),
                    Vertex::new(w, w, w),
                    Vertex::new(w, w, 0.0),
                ]),
                // Bottom
                Triangle::new([
                    Vertex::new(0.0, 0.0, w),
                    Vertex::new(0.0, 0.0, 0.0),
                    Vertex::new(w, 0.0, 0.0),
                ]),
                Triangle::new([
                    Vertex::new(0.0, 0.0, w),
                    Vertex::new(w, 0.0, 0.0),
                    Vertex::new(w, 0.0, w),
                ]),
            ],
        ),
    )
}
