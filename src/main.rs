extern crate piston_window;
use std::collections::HashSet;
use std::f64::consts::PI;

use piston_window::*;

mod colors;
mod math;
mod models;
mod world;

use math::{Point, Vector};
use models::Vertex;
use world::{cube, Entity};

const FOV: f64 = PI / 2.0;
const CAMERA_SPEED: f64 = 0.03;

struct Renderer<'a, 'b, G: Graphics> {
    size: Size,
    fov: f64,
    znear: f64,
    zfar: f64,
    context: Context,
    graphics: &'a mut G,
    camera: &'b Point,
}
impl<'a, 'b, G: Graphics> Renderer<'a, 'b, G> {
    fn new(
        size: Size,
        fov: f64,
        context: Context,
        graphics: &'a mut G,
        camera: &'b Point,
    ) -> Renderer<'a, 'b, G> {
        Renderer {
            size,
            fov,
            znear: 1.0,
            zfar: 10.0,
            context,
            graphics,
            camera,
        }
    }

    fn clear(&mut self) {
        clear(colors::BLACK, self.graphics);
    }

    fn camera_to_screen_coords(&self, vertices: &[[f64; 3]]) -> Vec<[f64; 2]> {
        vertices
            .iter()
            .map(|[x, y, _z]| {
                [
                    (x + 1.0) * 0.5 * self.size.width,
                    (-y + 1.0) * 0.5 * self.size.height,
                ]
            })
            .collect()
    }

    fn draw(&mut self, color: [f32; 4], vertices: &[[f64; 3]]) {
        let vertices = self.camera_to_screen_coords(vertices);

        polygon(
            color,
            vertices.as_slice(),
            self.context.transform,
            self.graphics,
        );
        // self.polygon_line(color, vertices.as_slice());
    }

    fn draw_line(&mut self, color: [f32; 4], vertices: &[[f64; 3]]) {
        let vertices = self.camera_to_screen_coords(vertices);

        self.polygon_line(color, vertices.as_slice());
    }

    fn project(&self, vertex: &Vertex) -> [f64; 3] {
        let [camx, camy, camz] = self.camera.as_slice();
        let [mut x, mut y, mut z] = vertex.as_slice();
        x -= camx;
        y -= camy;
        z -= camz;
        // TODO: Aspect ratio and q calculation only once per render loop
        let aspect_ratio = self.size.height / self.size.width;
        let q = self.zfar / (self.zfar - self.znear);

        let ez_inv = (self.fov * 0.5).tan();
        if z == 0.0 {
            z = 1.0;
        }
        let depth_scale = ez_inv * z;

        // let proj = Matrix::new(&[
        //     &[aspect_ratio / depth_scale, 0.0, 0.0, 0.0],
        //     &[0.0, 1.0 / depth_scale, 0.0, 0.0],
        //     &[0.0, 0.0, q, -self.znear * q],
        //     &[0.0, 0.0, 0.0, 1.0]
        // ]);
        [
            aspect_ratio * x / depth_scale,
            y / depth_scale,
            (z - self.znear) * q,
        ]
    }

    fn polygon_line(&mut self, color: [f32; 4], tri: &[[f64; 2]]) {
        let num_vertices = tri.len();
        for i in 0..num_vertices {
            let j = (i + 1) % num_vertices;
            line(
                color,
                1.0,
                [tri[i][0], tri[i][1], tri[j][0], tri[j][1]],
                self.context.transform,
                self.graphics,
            );
        }
    }
}

fn main() {
    let mut window: PistonWindow = WindowSettings::new("Cube!", [900, 500])
        .exit_on_esc(true)
        .build()
        .unwrap();
    let mut glyphs = window
        .load_font("/usr/share/fonts/truetype/oxygen/OxygenMono-Regular.ttf")
        .unwrap();

    let mut camera = Point::new(0.0, 0.0, 0.0);
    let mut fov = FOV;
    let mut objects: Vec<Entity> = vec![cube(0.0, 0.0, 3.0, 1.0)];
    let mut ticks = 0;
    let light = Vector::new(0.0, 0.0, -1.0);

    let mut keys = HashSet::new();
    let mut draw_normals = false;
    let mut events = Events::new(EventSettings::new().lazy(false));
    while let Some(event) = events.next(&mut window) {
        if let Some(Button::Keyboard(key)) = event.press_args() {
            if let Key::F = key {
                let [x, y, z] = camera.as_slice();
                objects.push(cube(*x, *y, z + 3.0, (ticks % 4 + 1) as f64));
            } else if let Key::N = key {
                draw_normals = !draw_normals;
            }
            keys.insert(key);
        }
        if let Some(Button::Keyboard(key)) = event.release_args() {
            keys.remove(&key);
        }

        if let Some(_) = event.update_args() {
            // Update camera
            for key in keys.iter() {
                match key {
                    Key::W => camera.translate_inplace(&Vector::new(0.0, CAMERA_SPEED, 0.0)),
                    Key::A => camera.translate_inplace(&Vector::new(-CAMERA_SPEED, 0.0, 0.0)),
                    Key::S => camera.translate_inplace(&Vector::new(0.0, -CAMERA_SPEED, 0.0)),
                    Key::D => camera.translate_inplace(&Vector::new(CAMERA_SPEED, 0.0, 0.0)),
                    Key::Q => camera.translate_inplace(&Vector::new(0.0, 0.0, CAMERA_SPEED)),
                    Key::E => camera.translate_inplace(&Vector::new(0.0, 0.0, -CAMERA_SPEED)),
                    Key::Equals => fov /= 1.001,
                    Key::NumPadMinus => fov *= 1.001,
                    Key::F | Key::N => (),
                    _ => println!("{:?}", key),
                }
            }

            let _count = objects.iter_mut().map(|o| o.update(ticks)).count();

            ticks += 1;
        }

        // Update screen
        if let Some(_) = event.render_args() {
            let size = window.size();
            window.draw_2d(&event, |context, graphics, device| {
                let mut renderer = Renderer::new(size, fov, context, graphics, &camera);

                renderer.clear();
                // Draw all world objects
                for (i, object) in objects.iter().enumerate() {
                    // Draw triangles
                    for triangle in object.triangles() {
                        let vertices = triangle.vertices();
                        let normal = Vector::between(&vertices[0], &vertices[1])
                            .cross(&Vector::between(&vertices[0], &vertices[2]))
                            .normalize();
                        // If the triangle is facing away from the camera, skip it
                        if normal.dot(&Vector::between(&camera, &vertices[0])) > 0.0 {
                            continue;
                        }

                        let normal_line = [
                            vertices[0].clone(),
                            Vertex::from(vertices[0].translate(&normal)),
                        ];
                        let vertices: Vec<[f64; 3]> = triangle
                            .vertices()
                            .iter()
                            .map(|v| renderer.project(&v))
                            .collect();

                        let brightness = normal.dot(&light) as f32;
                        let color = match i % 3 {
                            0 => colors::WHITE,
                            1 => colors::BLUE,
                            2 => colors::RED,
                            _ => unreachable!(),
                        };
                        let color = colors::scale_color(&color, brightness);
                        renderer.draw(color, vertices.as_slice());

                        if draw_normals {
                            // Debug the normal vector values
                            let normal_line: Vec<[f64; 3]> =
                                normal_line.iter().map(|v| renderer.project(v)).collect();
                            renderer.draw_line(colors::GREEN, &normal_line);
                        }
                    }
                }
                // Draw UI elements
                let [camx, camy, camz] = camera.as_slice();
                text::Text::new_color(colors::WHITE, 14)
                    .draw(
                        &format!(
                            "Fov: {:.2} - Camera: {:.2} {:.2} {:.2}",
                            fov / (PI) * 180.0,
                            camx,
                            camy,
                            camz
                        ),
                        &mut glyphs,
                        &context.draw_state,
                        context.transform.trans(0.0, 25.0),
                        graphics,
                    )
                    .unwrap();

                // Update glyphs before rendering.
                glyphs.factory.encoder.flush(device);
            });
        }
    }
}
