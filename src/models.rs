use std::ops::Deref;
use crate::math::Point;

#[derive(Clone, PartialEq, Debug)]
pub struct Vertex(Point);

impl Vertex {
    pub fn new(x: f64, y: f64, z: f64) -> Vertex {
        Vertex(Point::new(x, y, z))
    }

    pub fn as_slice(&self) -> &[f64; 3] {
        self.0.as_slice()
    }
}

impl Deref for Vertex {
    type Target = Point;

    fn deref(&self) -> &Point {
        &self.0
    }
}

impl From<Point> for Vertex {
    fn from(point: Point) -> Vertex {
        let [x, y, z] = point.as_slice();
        Vertex::new(*x, *y, *z)
    }
}

pub struct Triangle {
    vertices: [Vertex; 3],
}

impl Triangle {
    pub fn new(vertices: [Vertex; 3]) -> Triangle {
        Triangle { vertices }
    }

    pub fn vertices(&self) -> &[Vertex; 3] {
        &self.vertices
    }

    pub fn translate(&self, x: f64, y: f64, z: f64) -> Triangle {
        let mut vec: Vec<Vertex> = self.vertices().iter()
            .map(|vertex| {
                let [vx, vy, vz] = vertex.as_slice();
                Vertex::new(
                    vx + x,
                    vy + y,
                    vz + z
                )
            })
            .collect();
        let (v3, v2, v1) = (vec.pop().unwrap(), vec.pop().unwrap(), vec.pop().unwrap());
        Triangle::new([v1, v2, v3])
    }
}

pub struct Model {
    /// The center of the model should be at (0, 0, 0)
    triangles: Vec<Triangle>
}

impl Model {
    /// Create a new model where the center is located at (x, y, z) in model coordinates. This will
    /// be used to determine the rotation of the object.
    pub fn new(x: f64, y: f64, z: f64, triangles: &[Triangle]) -> Model {
        Model {
            // Translate the triangles
            triangles: triangles.iter().map(|t| t.translate(-x, -y, -z)).collect()
        }
    }

    pub fn triangles(&self) -> &[Triangle] {
        self.triangles.as_slice()
    }
}
