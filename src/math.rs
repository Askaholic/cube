use std::ops::{Index, Mul};
use std::slice::SliceIndex;

#[derive(Clone, PartialEq, Debug)]
struct Vector3f([f64; 3]);

impl Vector3f {
    pub fn as_slice(&self) -> &[f64; 3] {
        &self.0
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Matrix {
    rows: usize,
    cols: usize,
    data: Vec<Vec<f64>>,
}

impl Matrix {
    pub fn new(matrix: &[&[f64]]) -> Matrix {
        let rows = matrix.len();
        let cols = matrix[0].len();
        Matrix {
            rows,
            cols,
            data: matrix.iter().map(|row| row.to_vec()).collect(),
        }
    }

    pub fn from_vec(matrix: Vec<Vec<f64>>) -> Matrix {
        let rows = matrix.len();
        let cols = matrix[0].len();
        Matrix {
            rows,
            cols,
            data: matrix,
        }
    }

    pub fn mult_assign_ref(&mut self, other: &Matrix) {
        assert!(self.rows > 0);
        assert!(other.rows > 0);
        assert_eq!(self.cols, other.rows);

        let mut result_vec = vec![vec![0.0; other.cols]; self.rows];
        for i in 0..self.rows {
            for j in 0..other.cols {
                result_vec[i][j] = self.data[i]
                    .iter()
                    .enumerate()
                    .map(|(k, val)| val * other.data[k][j])
                    .sum();
            }
        }
        self.data = result_vec;
        self.cols = other.cols;
    }
}

impl<'a> Mul for &'a Matrix {
    type Output = Matrix;
    fn mul(self, other: &Matrix) -> Matrix {
        let mut ret = (*self).clone();
        ret.mult_assign_ref(other);
        ret
    }
}

impl<I: SliceIndex<[Vec<f64>], Output = Vec<f64>>> Index<I> for Matrix {
    type Output = Vec<f64>;

    fn index(&self, index: I) -> &Vec<f64> {
        &(self.data[index])
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Vector(Vector3f);

impl Vector {
    pub fn new(x: f64, y: f64, z: f64) -> Vector {
        Vector(Vector3f([x, y, z]))
    }

    pub fn between(p1: &Point, p2: &Point) -> Vector {
        let [x1, y1, z1] = p1.as_slice();
        let [x2, y2, z2] = p2.as_slice();
        Vector::new(x2 - x1, y2 - y1, z2 - z1)
    }

    pub fn as_slice(&self) -> &[f64; 3] {
        self.0.as_slice()
    }

    pub fn normalize(&self) -> Vector {
        let [x, y, z] = self.as_slice();
        let magnitude = (x * x + y * y + z * z).sqrt();
        Vector::new(x / magnitude, y / magnitude, z / magnitude)
    }

    pub fn cross(&self, other: &Vector) -> Vector {
        let [x1, y1, z1] = self.as_slice();
        let [x2, y2, z2] = other.as_slice();
        Vector::new(y1 * z2 - z1 * y2, z1 * x2 - x1 * z2, x1 * y2 - y1 * x2)
    }

    pub fn dot(&self, other: &Vector) -> f64 {
        let [x1, y1, z1] = self.as_slice();
        let [x2, y2, z2] = other.as_slice();
        x1 * x2 + y1 * y2 + z1 * z2
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Point(Vector3f);

impl Point {
    pub fn new(x: f64, y: f64, z: f64) -> Point {
        Point(Vector3f([x, y, z]))
    }

    pub fn as_slice(&self) -> &[f64; 3] {
        self.0.as_slice()
    }

    pub fn translate(&self, vector: &Vector) -> Point {
        let [x, y, z] = self.0.as_slice();
        let [vx, vy, vz] = vector.as_slice();
        Point::new(x + vx, y + vy, z + vz)
    }

    pub fn translate_inplace(&mut self, vector: &Vector) {
        let [vx, vy, vz] = vector.as_slice();
        unsafe {
            *(self.0).0.get_unchecked_mut(0) += vx;
            *(self.0).0.get_unchecked_mut(1) += vy;
            *(self.0).0.get_unchecked_mut(2) += vz;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn mat_mul_identity() {
        let identity = Matrix::new(&[&[1.0, 0.0], &[0.0, 1.0]]);
        let result = &identity * &identity;

        assert_eq!(result, identity);
    }

    #[test]
    fn mat_mul_identity_2() {
        let identity = Matrix::new(&[&[1.0, 0.0], &[0.0, 1.0]]);
        let result = &identity * &identity;

        assert_eq!(result, identity);
        let mat2 = Matrix::new(&[&[1123.0, 123.532], &[85931.0, 92384.1]]);
        let result = &identity * &mat2;

        assert_eq!(result, mat2);
    }

    #[test]
    fn mat_mul_identity_3() {
        let identity = Matrix::new(&[&[1.0, 0.0], &[0.0, 1.0]]);
        let vector = Matrix::new(&[&[1.0], &[2.0]]);
        let result = &identity * &vector;

        assert_eq!(result, vector);
    }

    #[test]
    fn mat_mul_identity_4() {
        let identity = Matrix::new(&[&[1.0, 0.0, 0.0], &[0.0, 1.0, 0.0], &[0.0, 0.0, 1.0]]);
        let vector = Matrix::new(&[&[1.0], &[2.0], &[3.0]]);
        let result = &identity * &vector;

        assert_eq!(result, vector);
    }

    #[test]
    fn mat_mul() {
        let mat1 = Matrix::new(&[
            &[14.0, 234.0, -234.23, 24.0],
            &[13.0, -24.0, 546.0, 796.0],
            &[54.0, 56.0, 856.0, 234.0],
        ]);
        let mat2 = Matrix::new(&[
            &[321.0, 246.0, 8777.0],
            &[135.0, 1276.0, 8237.0],
            &[765.0, 3456.0, 776.0],
            &[4021.0, 390.0, 103.0],
        ]);
        let result = &mat1 * &mat2;

        assert_eq!(
            result,
            Matrix::new(&[
                &[-46597.94999999998, -498110.88, 1871045.52],
                &[3619339.0, 2169990.0, 422097.0],
                &[1620648.0, 3134336.0, 1623588.0],
            ])
        );
    }

    #[test]
    fn vector_between() {
        assert_eq!(
            Vector::between(&Point::new(0.0, 0.0, 0.0), &Point::new(1.0, 1.0, 1.0)),
            Vector::new(1.0, 1.0, 1.0)
        );
        assert_eq!(
            Vector::between(&Point::new(1.0, 1.0, 1.0), &Point::new(1.0, 1.0, 1.0)),
            Vector::new(0.0, 0.0, 0.0)
        );
        assert_eq!(
            Vector::between(&Point::new(1.0, 1.0, 1.0), &Point::new(0.0, 0.0, 0.0)),
            Vector::new(-1.0, -1.0, -1.0)
        );
    }

    #[test]
    fn vector_cross() {
        assert_eq!(
            Vector::new(1.0, 0.0, 0.0).cross(&Vector::new(0.0, 0.0, 1.0)),
            Vector::new(0.0, 1.0, 0.0)
        );
    }
}
